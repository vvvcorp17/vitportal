<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyEnum;
use App\Traits\Owner;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $items = Property::with('enums')
            ->get();

        return view('admin.properties-list', compact('items'));
    }

    public function byCategory(Category $category)
    {
        return Property::category($category->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $property = new Property();
        $property = $this->propertyCollect($property, $request);
        $property->save();

        $this->saveEnums($property, $request);

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * @param Property $property
     * @param Request  $request
     */
    public function propertyCollect(Property $property, Request $request)
    {
        $property->title = $request->title;
        $property->type  = $request->type;
        $property->filter  = (bool)$request->filter;
        $property->card  = (bool)$request->card;

        return $property;
    }

    public function saveEnums(Property $property, Request $request)
    {
        PropertyEnum::where('property_id', $property->id)->delete();

        if($request->type === 'enum') {
            foreach ($request->enums as $enum) {
                if(!$enum) continue;

                $propertyEnum = new PropertyEnum([
                     'property_id' => $property->id,
                     'value' => $enum,
                 ]);

                $propertyEnum->save();
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return RedirectResponse
     */
    public function update(Property $property, Request $request)
    {
        $property = $this->propertyCollect($property, $request);
        $property->save();

        $this->saveEnums($property, $request);

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Property $property
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Property $property)
    {
        $property->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
