<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Entity;
use App\Models\Review;
use App\Policies\Entity\EntityPolicy;
use App\Policies\Entity\ShowEntityPolicy;
use App\Traits\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EntityController extends Controller
{
    public function index(Request $request, Category $category)
    {
        $entities = Entity::byCategory($category->id)
            ->with('reviews')
            ->filter($request)
            ->sort('rate')
            ->paginate(9);

        // dd($entities);

        return view('entity.index', compact('entities', 'category'));
    }

    public function show(Category $category, $entity_id)
    {
        $entity_id = (int)$entity_id;

        $entity = Entity::where(['category_id' => $category->id, 'id' => $entity_id])
            ->firstOrFail();

        $reviews = Review::where('entity_id', $entity->id)
            ->with('user')
            ->get();

        return view('entity.show', compact('entity', 'category', 'reviews',));
    }

    public function store(EntityPolicy $request, Category $category)
    {
        $validated = $request->allow();

        $entity = new Entity($validated);
        $entity->category_id = $category->id;
        $entity->user_id = Auth::user()->id;
        $entity->properties = isset($validated['properties']) ? $validated['properties'] : [];
        $entity->save();

        $this->uploadImg($validated, $entity);

        return redirect()->back()->with('message', 'Added');
    }

    public function update(EntityPolicy $request, Category $category, Entity $entity)
    {
        Owner::checkUserId($entity);

        $validated = $request->allow();

        $entity->fill($validated);
        $entity->save();
        $this->uploadImg($validated, $entity);

        return redirect()->back()->with('message', 'Updated');
    }

    public function uploadImg($validated, $entity) {
        if (isset($validated['images'])) {
            if ($files = $validated['images']) {
                foreach ($files as $key => $file) {
                    $name = time() . '-' . $key .'.jpg';
                    $file->move('uploads/entity/' . $entity->id, $name);
                }
            }
        }

        if (isset($validated['mainImage'])) {
            if ($file = $validated['mainImage']) {
                $name = 'image.jpg';
                $file->move('uploads/entity/' . $entity->id . '/main', $name);
            }
        }
    }
    public function removeImg(Entity $entity, Request $request)
    {
        Owner::checkUserId($entity);

        Storage::disk('public')->delete($request->image);

        return response('ok', 200);
    }

    public function destroy(Category $category, Entity $entity)
    {
        Owner::checkUserId($entity);

        Storage::disk('public')->deleteDirectory('uploads/entity/' . $entity->id);

        $entity->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
