<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class AdminController extends Controller
{
    public function index()
    {
        $items = User::all();

        return view('admin.users-manage', compact('items'));
    }
}
