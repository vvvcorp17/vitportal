<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Owner;

class UserManageController extends Controller
{
    public function index()
    {
        $items = User::all();

        return view('admin.users-manage', compact('items'));
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
