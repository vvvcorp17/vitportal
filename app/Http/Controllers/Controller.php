<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

        // $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $user = Auth::user();
            $userId = $user ? $user->id : 0;

            View::share('user', $user);
            View::share('userId', $userId);

            return $next($request);
        });
    }
}
