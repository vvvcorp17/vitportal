<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Property;
use App\Models\Review;
use App\Models\PropertyValue;
use App\Traits\Owner;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param         $category_slug
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View
     */
    public function index(Request $request, $category_slug)
    {
        $category = Category::where('slug', $category_slug)
            ->firstOrFail();

        // $productCategories = Category::where('parent_id', $category->parent_id)->get();

        $filters = PropertyValue::select('property_values.id', 'property_values.value', 'property_values.property_id')
            ->leftJoin('products', 'products.id', '=', 'property_values.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('properties', 'properties.id', '=', 'property_values.property_id')
            ->where('categories.id', $category->id)
            ->get();

        $propertiesByCategory = Property::category($category->id)->get();

        $products = Product::
        with('property_values.property')
            ->where('category_id', $category->id)
            // ->join('property_values', 'products.id', '=', 'property_values.product_id')
            ->limit(21);

        // DB::enableQueryLog();

        $requestFilters = $request->get('filter');
        if ($requestFilters) {
            // dump($requestFilters);

            foreach ($requestFilters as $property => $filter) {
                if (!$filter) {
                    continue;
                }

                $products->whereHas(
                    'property_values',
                    function ($query) use ($filter, $property) {
                        $query->where('property_values.property_id', $property);
                        $query->where('property_values.value', 'like', '%' . $filter . '%');

                        return $query;
                    }
                );
            }
        }

        $filterRange = $request->get('filterRange');

        if ($filterRange) {
            foreach ($filterRange as $key => $filter) {
                if (!$filter['from'] && !$filter['to']) {
                    continue;
                }

                $products->whereHas(
                    'property_values',
                    function ($query) use ($filter, $key) {
                        $query->where('property_values.property_id', $key);
                        $query->where('property_values.value', '>=', $filter['from'] ?: 0);
                        $query->where('property_values.value', '<=', $filter['to'] ?: 999999);

                        return $query;
                    }
                );
            }
        }

        $products = $products
            ->groupBy('products.id')
            ->get();

        // dump(DB::getQueryLog());
        // dd();

        return view('products.products', compact('category', 'products', 'filters', 'propertiesByCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $product = new Product($request->all());

        $product->user_id = Auth::user()->id;
        $product->save();

        if ($request->image) {
            if ($file = $request->file('image')) {
                $file->move('uploads/products/', $product->id . '.png');
            }
        }

        if ($request->property_values) {
            $this->saveProperty_values($product, $request->property_values);
        }

        return redirect()->back()->with('message', 'Created');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     *
     * @return View
     */
    public function show(Product $product)
    {
        $reviews = Review::whereHas(
            'product',
            function ($query) use ($product) {
                return $query->where('products.id', $product->id);
            }
        )
            ->with('user')
            ->get();

        return view('products.product', compact('product', 'reviews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Product                  $product
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        Owner::checkUserId($product);

        $product->title = $request->title;

        if ($request->category_id) {
            $product->category_id = $request->category_id;
        }

        $product->save();

        if ($request->image) {
            if ($file = $request->file('image')) {
                $file->move('uploads/products/', $product->id . '.png');
            }
        }

        if ($request->property_values) {
            $this->saveProperty_values($product, $request->property_values);
        }

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * @param mixed       $product
     * @param array|mixed $property_values
     *
     * @return \Illuminate\Auth\Access\Response|void
     */
    public function saveProperty_values($product, $property_values)
    {
        foreach ($property_values as $property_id => $value) {
            if (!$value) {
                continue;
            }

            $property = Property::findOrFail($property_id);

            $valueModel = PropertyValue::firstOrCreate(
                [
                    'property_id' => $property_id,
                    'product_id'  => $product->id,
                ],
                [
                    'property_id' => $property_id,
                    'product_id'  => $product->id,
                ]
            );

            if ($valueModel) {
                $valueModel->value = $value;
                $valueModel->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product)
    {
        Owner::checkUserId($product);

        $product->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
