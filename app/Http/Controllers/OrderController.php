<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Traits\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $myOrders = Order::where('user_id', Auth::user()->id)
            ->with('entity')
            ->get();

        $clientOrders = Order::whereHas('entity', function ($query) {
                return $query->where('entities.user_id', Auth::user()->id);
            })
            ->get();

        return view('profile.orders', compact('myOrders', 'clientOrders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->from_date = $request->from_date;
        $order->to_date = $request->to_date;
        $order->note = $request->note;

        $order->entity_id = $request->entity_id;

        $order->save();

        return redirect()->back()->with('message', 'Order Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Order $order)
    {
        $owner = Order::with('entity')
            ->where('orders.id', $order->id)
            ->leftJoin('entities', 'orders.entity_id', '=', 'entities.id')
            ->where(function ($query) {
                $query->where('entities.user_id', Auth::user()->id)
                    ->orWhere('orders.user_id', Auth::user()->id);
            })
            ->firstOrFail();

        $order->status = $request->status;
        $order->save();

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Order $order)
    {
        Owner::checkUserId($order);

        $order->delete();

        return redirect()->back();
    }
}
