<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Review;
use App\Traits\Owner;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $restaurantsQuery = Company::
        with('reviews')
            ->where(function ($query) use ($request) {
                if ($request->query('query')) {
                    $query->where('title', 'like', '%' . $request->query('query') . '%');
                }

                return $query;
            });

        if($request->query()) {
            $restaurantsQuery
                ->whereHas('halls', function ($query) use ($request) {
                    if($request->query('price')) {
                        $query
                            ->where('halls.price_from', '<=', $request->query('price'))
                            ->where('halls.price_to', '>=', $request->query('price'));
                    }

                    if($request->query('people_count')) {
                        $query
                            ->where('halls.people_from', '<=', $request->query('people_count'))
                            ->where('halls.people_to', '>=', $request->query('people_count'));
                    }

                    return $query;
                });
        }

        if ($request->get('sorting') === 'new') {
            $restaurantsQuery
                ->orderBy('created_at', 'desc');
        }

        if ($request->get('sorting') === 'rate') {
            $restaurantsQuery
                ->withCount(['reviews as average_rating' => function($query) {
                    $query->select(DB::raw('coalesce(avg(rate),0)'));
                }])->orderBy('average_rating', 'desc');
        }

        // dd($restaurantsQuery->toSql());


        $restaurants = $restaurantsQuery->get();

        return view('restaurants/restaurants', compact('restaurants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required',
        ]);

        $restaurant = new Company();
        $restaurant = $this->collectRestaurant($request, $restaurant);

        $restaurant->user_id = Auth::user()->id;

        $restaurant->save();

        return redirect()->back()->with('message', 'Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Company      $restaurant
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Company $restaurant, Request $request)
    {
        Owner::checkUserId($restaurant);

        if($restaurant->user_id !== Auth::user()->id) {
            return redirect('503');
        }

        $restaurant = $this->collectRestaurant($request, $restaurant);
        $restaurant->save();

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * @param $request
     * @param $restaurant
     * @return mixed
     */
    private function collectRestaurant($request, $restaurant) {

        $restaurant->title = $request->title;
        $restaurant->phone = $request->phone;
        $restaurant->address = $request->address;
        $restaurant->price = $request->price;
        $restaurant->short_description = $request->short_description;
        $restaurant->description = $request->description;

        if ($request->image) {

            if($restaurant->image) {
                Storage::disk('public')->delete($restaurant->image);
            }

            if ($file = $request->file('image')) {
                $name = time() . '-' . $file->getClientOriginalName();
                $file->move('uploads/restaurants/', $name);
                $restaurant->image = 'uploads/restaurants/' . $name;
            }
        }

        return $restaurant;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response|int
     */
    public function removeImg(Request $request) {

        $restaurant = Company::where('user_id', Auth::user()->id)
            ->where('id', $request->restaurant)
            ->firstOrFail();

        if ($restaurant->image === $request->image) {
            if (Storage::disk('public')->delete($restaurant->image)) {
                $restaurant->image = '';
                $restaurant->save();
            }
        }

        return response('ok', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company $restaurant
     */
    public function show(Company $restaurant)
    {
        $reviews = Review::whereHas('restaurant',function ($query) use ($restaurant) {
            return $query->where('restaurants.id', $restaurant->id);
        })
            ->with('user')
            ->get();


        return view('restaurants/restaurant', compact('restaurant', 'reviews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company $restaurant
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $restaurant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Company $restaurant
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Company $restaurant)
    {
        Owner::checkUserId($restaurant);

        Storage::disk('public')->delete($restaurant->image);

        $restaurant->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
