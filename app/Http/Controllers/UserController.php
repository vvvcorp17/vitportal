<?php

namespace App\Http\Controllers;

use App\Models\Entity;
use App\Models\User;
use App\Traits\UploadTrait;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class UserController extends Controller
{
    use UploadTrait;

    public function showForm(Request  $request)
    {
        return view('profile.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect('/');
        } else {
            return Redirect::back()->withErrors('Invalid Login / Password');
        }
    }

    public function register(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'business' => 'nullable|numeric'
        ]);

        $user = User::create($validated);

        auth()->login($user);

        return redirect()->to('/profile-settings');
    }

    public function profileForm()
    {
        return view('profile.settings');
    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();

        $data = $this->validate(request(), [
            'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'country' => 'required',
            'location' => 'required',
        ]);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->country = $data['country'];
        $user->location = $data['location'];

        if ($request->has('avatar')) {
            // Get image file
            $image = $request->file('avatar');
            // Make a image name based on user name and current timestamp
            $name = Str::slug($request->input('email'));
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->deleteOne(Auth::user()->avatar, 'public');
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath

            $user->avatar = $filePath;
        }

        $user->save();

        return redirect()->back()->with('message', 'Updated');
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => $password
                ])->save();

                $user->setRememberToken(Str::random(60));

                event(new PasswordReset($user));
            }
        );

        return $status == Password::PASSWORD_RESET
            ? redirect('/login')->withErrors($status . ' success')
            : back()->withErrors(['email' => __($status)]);
    }

    public function resetPasswordSend(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? redirect()->to('/login')->withErrors('Ссылка для восстановления пароля была отправлена на Email')
            : back()->withErrors(['email' => __($status)]);
    }

    public function changePasswordForm(Request $request)
    {
        return view('profile.change-password');
    }

    public function changePassword(Request $request)
    {
        $password = $request->validate([
            'old-password' => 'required',
            'password' => 'required|confirmed',
        ]);

        $user = Auth::user();

        if (!Hash::check($password['old-password'], $user->password)) {
            return back()->withErrors('Не правильный старый пароль');
        }


        $user->password = $password['password'];
        $user->save();

        return back()->withInput($password)->with('message', 'Success');
    }

    public function myBusiness() {

        $entities = Entity::where('user_id', Auth::user()->id)
            ->with('category')
            ->orderBy('category_id')
            ->orderBy('created_at', 'desc')
            ->limit(50)
            ->get();

        return view('profile.my-business', compact('entities'));
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->to('/');
    }
}
