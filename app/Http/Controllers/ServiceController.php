<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\Service;
use App\Traits\Owner;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $category
     * @return View
     */
    public function index($category)
    {
        $services = Service::
            with('reviews')
            ->with('category')
            ->whereHas('category', function ($query) use ($category) {
                return $query->where('categories.slug', $category);
            })
            ->get();

        return view('services.services', compact('services', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $service = new Service();

        $service->user_id = Auth::user()->id;

        $this->collectService($service, $request);

        return redirect()->back()->with('message', 'Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Service $service)
    {
        Owner::checkUserId($service);

        $this->collectService($service, $request);

        return redirect()->back()->with('message', 'Updated');
    }

    private function collectService($service, $request)
    {
        $service->title = $request->title;
        $service->phone = $request->phone;
        $service->price = $request->price;
        $service->category_id = $request->category_id;
        $service->short_description = $request->short_description;
        $service->description = $request->description;

        $service->links = $request->links;

        $service->save();

        if ($files = $request->file('images')) {
            foreach ($files as $key => $file) {
                $name = $service->id . '-' .  $key . time() . '.' . $file->extension();
                $file->move('uploads/services/', $name);
                $images[] = 'uploads/services/' . $name;
            }
        }

        if ($file = $request->file('image')) {

            $mainImage = $service->getMainImage();

            if($mainImage) {
                Storage::disk('public')->delete($mainImage);
            }

            $name = $service->id . '-main-' .  time() . '.' .$file->extension();
            $file->move('uploads/services/', $name);
            $images[] = 'uploads/services/' . $name;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show(Service $service)
    {
        $reviews = Review::where('service_id', $service->id)
            ->with('user')
            ->get();

        return view('services.service', compact('service', 'reviews',));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Service $service)
    {
        Owner::checkUserId($service);

        $service->removeImages();
        $service->delete();

        return redirect()->back()->with('message', 'Deleted');
    }
}
