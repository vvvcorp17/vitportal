<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Entity;
use App\Models\Product;
use App\Models\Company;
use App\Models\Service;
use App\Traits\Telegram;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $halls = Entity::with('reviews', 'category')
            ->where('category_id', 1)
            ->limit(5)
            ->get();

        $services = Entity::with('reviews', 'category')
            ->whereIn('category_id', [3, 4])
            ->limit(5)
            ->get();

        $products = Entity::with('reviews', 'category')
            ->whereIn('category_id', [6, 7])
            ->limit(5)
            ->get();

        return view('index', compact('halls', 'services', 'products',));
    }
}
