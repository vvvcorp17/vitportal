<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if (!Auth::user() || Auth::user()->role !== 'admin') {
            return response('Not found', 404);
        }

        return $next($request);
    }

}
