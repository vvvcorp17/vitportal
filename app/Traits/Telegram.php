<?php
namespace App\Traits;

use App\Models\Category;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait Telegram
{
    public static function send(string $text)
    {
        return Http::post(
            'https://api.telegram.org/bot1522173165:AAFLr0UEV9tisgRq0Pb_uYWj6IJQZy0ZcZA/sendMessage?chat_id=523159028&text=' . $text
        );
    }
}
