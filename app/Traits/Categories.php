<?php
namespace App\Traits;

use App\Models\Category;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait Categories
{
    public static function getAll()
    {
        return Category::all();
    }

    public static function getSubCategories($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();

        return Category::where('parent_id', $category->id)->get();
    }

    public static function getCategoryBySlug($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();

        return $category;
    }
}
