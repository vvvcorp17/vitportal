<?php
namespace App\Traits;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait Owner
{
    public static function checkUserId($resource, $userId = null)
    {
        if (!Auth::user() || Auth::user()->id !== $resource->user_id) {
            abort(503);
        }
    }
}
