<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public const TYPES = [
        'number',
        'float',
        'string',
        'text',
        'date',
        'enum',
    ];

    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_properties');
    }

    public function scopeCategory($query, $category_id)
    {
        return $query->leftJoin('category_properties', 'category_properties.property_id', '=', 'properties.id')
            ->where('category_properties.category_id', '=', $category_id);
    }

    public function properties()
    {
        return $this->belongsToMany(Category::class, 'category_properties');
    }

    public function enums()
    {
        return $this->hasMany(PropertyEnum::class);
    }
}
