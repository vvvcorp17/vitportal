<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'category_properties');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function props()
    {
        return $this
            ->belongsToMany(Property::class, 'category_properties')
            ->wherePivot('category_id', $this->id);
    }
}
