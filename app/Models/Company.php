<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public function reviews()
    {
        return $this->belongsTo(Review::class);
    }

    public function halls() {
        return $this->hasMany(Entity::class, 'restaurant_id');
    }

    public function getMainImage() {
        return $this->images ? '/' . json_decode($this->images, true)[0] : 'assets/img/sample.jpg';
    }
}
