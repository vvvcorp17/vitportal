<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Entity extends Model
{
    use HasFactory;

    protected $guarded = ['id', 'user_id'];
    protected $casts   = ['properties' => 'array'];

    public function getImages(int $number = 25)
    {
        $images = Storage::disk('public')->files('uploads/entity/' . $this->id);

        return array_slice($images, 0, $number);
    }

    public function getMainImage()
    {
        $image = Storage::disk('public')->exists('uploads/entity/' . $this->id . '/main/image.jpg');

        return $image ? "/uploads/entity/$this->id/main/image.jpg" : 'https://picsum.photos/295/170?random=' . $this->id;
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'entity_id');
    }

    public function property_values()
    {
        return $this->hasMany(PropertyValue::class, 'entity_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeByCategory(Builder $query, $category)
    {
        return $query->where('category_id', $category);
    }

    public function scopeSort(Builder $query, ?string $sorting = null)
    {
        switch ($sorting) {
            case 'rate':
                $query->withCount(
                    [
                        'reviews as average_rating' => function ($query) {
                            $query->select(DB::raw('coalesce(avg(rate),0)'));
                        },
                        'reviews as count' => function ($query) {
                            $query->select(DB::raw('count(rate)'));
                        },
                    ]
                );
                $query->orderBy('average_rating', 'desc');
                $query->orderBy('count', 'desc');
                break;
            case 'new':
                $query->orderBy('created_at');
                break;
            case 'pricelow':
                echo "i равно 2";
                break;
            case 'pricehigh':
                echo "i равно 3";
                break;
        }

        return $query->orderBy('id');
    }

    public function scopeFilter(Builder $query, Request $request)
    {
        if (!$request->query->count()) {
            return $query;
        }

        foreach ((array)$request->query->get('filter') as $id => $value) {
            if (!$value) {
                continue;
            }

            $query->where("properties->$id", 'ILIKE', '%' . $value . '%');
        }

        foreach ((array)$request->query->get('filterRange') as $id => $value) {
            if (!isset($value['from']) && !isset($value['to'])) {
                continue;
            }

            $query->whereRaw(
                "(properties->>'$id')::int >= ?",
                [
                    $value['from'] ?? 0,
                ]
            );

            $query->whereRaw(
                "(properties->>'$id')::int <= ?",
                [
                    $value['to'] ?? 999999,
                ]
            );
        }

        foreach ((array)$request->query->get('filterIn') as $id => $value) {
            if (!count($value)) {
                continue;
            }

            $query->whereJsonContains("properties->prop$id", (array)$value);
        }

        if ($request->query->get('priceFrom') || $request->query->get('priceTo')) {
            $priceFrom = $request->priceFrom ?? 0;
            $priceTo   = $request->priceTo ?? 999999;

            $query->where('price', '>=', $priceFrom)
                ->where('price', '<=', $priceTo);
        }

        if ($request->title) {
            $query->where('title', 'ILIKE', '%' . $request->title . '%');
        }

        return $query;
    }
}
