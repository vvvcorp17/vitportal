<?php

namespace App\Policies\Entity;

use App\Models\Property;
use Illuminate\Contracts\Validation\Rule;

class PropertyPolicy implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $countOfProperties = Property::whereIn('id', array_keys($value))->count();

        return $countOfProperties === count($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute doesn\'t exists.';
    }
}
