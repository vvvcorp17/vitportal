<?php

namespace App\Policies\Entity;

use Illuminate\Support\Facades\Validator;

class EntityPolicy
{
    public function allow()
    {
        $validated = Validator::make(
            request()->all(),
            [
                'entity_id' => ['sometimes', 'nullable'],
                'title' => ['required', 'string', 'max:128'],
                'price' => ['required', 'numeric'],
                'short_description' => ['sometimes', 'nullable', 'string', 'max:200'],
                'description' => ['sometimes', 'nullable', 'string',],
                'properties' => ['array', new PropertyPolicy()],
                'images.*' => [],
                'mainImage' => 'sometimes|mimes:jpeg,jpg,png',
            ]
        )->validate();

        return $validated;
    }
}
