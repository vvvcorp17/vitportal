<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testStore()
    {
        $this->json('POST', '/register', [
            'name' => 'test',
            'role' => 'admin',
            'email' => 'test@mail.ru',
            'password' => 'test@mail.ru',
            'password_confirmation' => 'test@mail.ru',
            'business' => 1,
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'test@mail.ru'
        ]);
    }

    public function testShow()
    {
        $user = User::find(1);

        $res = $this->actingAs($user)->json('POST', '/profile-settings', [
            'name' => 'test2',
            'email' => 'test@mail.ru',
            'phone' => '1231243223',
            'country' => 'MD',
            'location' => 'location',
        ]);

        $this->assertDatabaseHas('users', [
            'name' => 'test2'
        ]);
    }
}
