<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class RestaurantTest extends TestCase
{
    use DatabaseMigrations;

    public function testStore()
    {
        $user = \App\Models\User::factory(User::class)->create();

        $res = $this->actingAs($user)->json('POST', '/restaurants', [
            'title' => 'test',
            'phone' => '123123',
            'address' => 'address',
            'price' => '123',
            'short_description' => 'rqwerqwe',
        ]);

        $this->assertEquals($res->status(), 302);

        $this->assertDatabaseHas('restaurants', [
            'title' => 'test'
        ]);
    }

    public function testShow()
    {
        $user = \App\Models\User::factory(User::class)->make();

        $restaurant = Company::all();

        dd($restaurant);

        $res = $this->actingAs($user)->json('PUT', '/restaurants/' . $restaurant->id, [
            'title' => 'test2',
            'phone' => '123123',
            'address' => 'address',
            'price' => '123',
            'short_description' => 'rqwerqwe',
        ]);

        $this->assertEquals($res->status(), 302);

        $this->assertDatabaseHas('restaurants', [
            'title' => 'test2'
        ]);
    }
}
