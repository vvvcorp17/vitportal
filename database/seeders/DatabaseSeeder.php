<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Entity;
use App\Models\Property;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DefaultSeeder::class,
        ]);

        Property::factory()->count(5)->create();
        Entity::factory()->count(2)->create();

        foreach (Property::all() as $property) {
            foreach (Category::all() as $category) {
                $category->properties()->attach($property);
            }
        }
    }
}
