<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Банкетные залы',
            'slug' => 'halls',
        ]);

        DB::table('categories')->insert([
            'title' => 'Услуги',
            'slug' => 'services',
        ]);

        DB::table('categories')->insert([
            'title' => 'Фотографы',
            'slug' => 'photographers',
            'parent_id' => '2',
        ]);

        DB::table('categories')->insert([
            'title' => 'Видео операторы',
            'slug' => 'video-operators',
            'parent_id' => '2',
        ]);

        DB::table('categories')->insert([
            'title' => 'товары',
            'slug' => 'products',
        ]);

        DB::table('categories')->insert([
            'title' => 'Платья',
            'slug' => 'dressess',
            'parent_id' => '5',
        ]);

        DB::table('categories')->insert([
            'title' => 'Кольца',
            'slug' => 'rings',
            'parent_id' => '5',
        ]);

        DB::table('users')->insert([
            'name' => 'vvv',
            'role' => 'admin',
            'business' => 1,
            'email' => 'neoninza3@gmail.com',
            'password' => '$2y$10$zj6N9dkTr9BHJLbM.OxwD.NygusEGSmjVJw4zZ8oCrh2tpyBUH7w2',
        ]);
    }
}
