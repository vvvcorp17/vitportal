<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Entity extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Entity::factory()
            ->count(5000)
            ->create();
    }
}
