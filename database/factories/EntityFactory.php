<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Entity;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EntityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Entity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $properties = [];
        $category = $this->faker->randomElement([1,3,4,6,7]);

        foreach (Property::all() as $property) {
            $properties[$property->id] = $this->faker->{$property->type == 'number' ? 'randomDigit' : 'title'};
        }

        return [
            'user_id' => 1,
            'category_id' => $category,
            'title' => $this->faker->title,
            'price' => $this->faker->randomDigit,
            'short_description' => $this->faker->text(120),
            'description' => $this->faker->text(500),
            'properties' => $properties,
        ];
    }
}
