<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');

Route::get('/login', [App\Http\Controllers\UserController::class , 'showForm'])->name('login');
Route::post('/login', [App\Http\Controllers\UserController::class , 'login'])->name('login');
Route::get('/register', function () {return view('profile.register');})->name('register');
Route::post('/register', [App\Http\Controllers\UserController::class , 'register']);
Route::get('/logout', [App\Http\Controllers\UserController::class , 'logout'])->middleware('auth');

Route::get('/profile-settings', [App\Http\Controllers\UserController::class , 'profileForm'])->middleware('auth');
Route::post('/profile-settings', [App\Http\Controllers\UserController::class , 'profileUpdate'])->middleware('auth');

Route::get('/change-password', [App\Http\Controllers\UserController::class , 'changePasswordForm'])->middleware('auth');
Route::post('/change-password', [App\Http\Controllers\UserController::class , 'changePassword'])->middleware('auth');

Route::get('/forgot-password', function () {return view('profile.forgot-password');});
Route::post('/forgot-password', [App\Http\Controllers\UserController::class , 'resetPasswordSend']);
Route::get('/reset-password/{token}', function ($token) { return view('profile.restore-password', ['token' => $token]);})->name('password.reset');
Route::post('/forgot-password-new', [App\Http\Controllers\UserController::class , 'resetPassword']);

Route::resource('/reviews', App\Http\Controllers\ReviewController::class);
Route::resource('/orders', App\Http\Controllers\OrderController::class);

Route::get('/entities/{category}', [App\Http\Controllers\EntityController::class, 'index']);
Route::get('/entities/{category}/{entity_id}', [App\Http\Controllers\EntityController::class, 'show']);
Route::post('/entities/{category}', [App\Http\Controllers\EntityController::class, 'store'])->middleware('auth');
Route::post('/entities/{entity}/remove-img', [App\Http\Controllers\EntityController::class, 'removeImg'])->middleware('auth');
Route::put('/entities/{category}/{entity}', [App\Http\Controllers\EntityController::class, 'update'])->middleware('auth');
Route::delete('/entities/{category}/{entity}', [App\Http\Controllers\EntityController::class, 'destroy'])->middleware('auth');

Route::get('/my-business', [App\Http\Controllers\UserController::class , 'myBusiness'])->middleware('auth');
Route::get('/my-orders', [App\Http\Controllers\UserController::class , 'myOrders'])->middleware('auth');

Route::middleware(['admin'])->group(function () {
    Route::resource('/admin/', App\Http\Controllers\Admin\AdminController::class);
    Route::resource('/admin/categories', App\Http\Controllers\Admin\CategoryManageController::class);
    Route::resource('/admin/users', App\Http\Controllers\Admin\UserManageController::class);
    Route::resource('/admin/properties', App\Http\Controllers\PropertyController::class);
    Route::get('/properties/byCategory/{category}', [App\Http\Controllers\PropertyController::class, 'byCategory']);
});

