@extends('layout.mainlayout')
@section('content')
        <!-- Home Banner -->
        <section class="section section-search">
            <div class="container-fluid">
                <div class="banner-wrapper">
                    <div class="banner-header text-center">
                        <h1 class="text-border">Все необходимое для вашей свадьбы</h1>
                        <p>Организуйте самое оригинальное и неповторимое свадебное торжество!</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section pt-5">
            <div class="container-fluid">
                <h2>Банкетные залы</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="doctor-slider slider">
                            @foreach($halls as $entity)
                                @include('entity.entity')
                            @endforeach
                        </div>
                    </div>
               </div>
            </div>
        </section>

        <section class="section pt-5">
            <div class="container-fluid">
                @if (count($services))
                    <h2>Услуги</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="doctor-slider slider">
                                @foreach($services as $entity)
                                    @include('entity.entity')
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>

       <section class="section pt-5">
            <div class="container-fluid">
                @if (count($products))
                    <h2 class="mt-2">Аксессуары</h2>
                    <div class="row">
                        <div class="col-md-12 features-img">
                            <div class="doctor-slider slider">
                                @foreach($products as $entity)
                                    @include('entity.entity')
                                @endforeach
                            </div>
                        </div>
                   </div>
                @endif
            </div>
        </section>
@endsection

