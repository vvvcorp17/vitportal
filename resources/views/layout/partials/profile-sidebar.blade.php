 <div class="profile-sidebar">
        <div class="widget-profile pro-widget-content">
            <div class="profile-info-widget">
                <a href="#" class="booking-doc-img">
                    <img src="{{ $user->avatar ?: '/assets/img/sample.jpg' }}" alt="User Image">
                </a>
                <div class="profile-det-info">
                    <h3>{{ $user->name }}</h3>
                    <div class="patient-details">
                        <h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> {{ $user->country }} {{ $user->location }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-widget">
            <nav class="dashboard-menu">
                <ul>
                    @if($user->business)
                        <li class="">
                            <a href="/my-business">
                                <i class="fas fa-dollar-sign"></i>
                                <span>Администрирование</span>
                            </a>
                        </li>
                    @endif
                    <li class="">
                        <a href="/orders">
                            <i class="fas fa-dollar-sign"></i>
                            <span>Заказы</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/profile-settings">
                            <i class="fas fa-user-cog"></i>
                            <span>Настройки профиля</span>
                        </a>
                    </li>
                    <li>
                        <a href="/change-password">
                            <i class="fas fa-lock"></i>
                            <span>Сменить пароль</span>
                        </a>
                    </li>
                    <li>
                        <a href="/logout">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>Выход</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </div>
