    <!-- Footer -->
	<footer class="footer">

				<!-- Footer Bottom -->
                <div class="footer-bottom">
					<div class="container-fluid">

						<!-- Copyright -->
						<div class="copyright">
							<div class="row">
								<div class="col-md-6 col-lg-6">
									<div class="copyright-text">
										<p class="mb-0">&copy; 2021 Weddi. All rights reserved.</p>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">

									<!-- Copyright Menu -->
									<div class="copyright-menu">
										<ul class="policy-menu">
											<li><a href="" data-toggle="modal" data-target="#feedback" class="feedback">Обратная связь</a></li>
										</ul>
									</div>
									<!-- /Copyright Menu -->


									<div id="feedback" class="modal custom-modal fade" role="dialog" style="display: none;" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Обраная связь</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">×</span>
													</button>
												</div>
												<div class="modal-body">
													<h6>Появились вопросы, предложения, трудности? </h6>
													<form action="https://formspree.io/f/mvovnwnl" method="post">
														@csrf
														<div class="form-group card-label form-focus">
															<label>Имя</label>
															<input class="form-control " autocomplete="off" name="name" type="text">
														</div>
														<div class="form-group card-label form-focus">
															<label>Email</label>
															<input required class="form-control " autocomplete="off" name="email" type="email">
														</div>
														<div class="form-group card-label">
															<label>текст</label>
															<textarea required class="form-control" name="body" type="text"> </textarea>
														</div>
														<div class="submit-section">
															<button class="btn btn-primary submit-btn">Отправить</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!-- /Copyright -->

					</div>
				</div>
				<!-- /Footer Bottom -->

			</footer>
			<!-- /Footer -->
