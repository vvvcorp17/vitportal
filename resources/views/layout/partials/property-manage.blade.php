<div class="modal fade" id="manage_property_modal{{ $item->id ?? '' }}" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-content p-2 ">
                    <form action="/admin/properties/{{ $item->id ?? '' }}" method="post">
                        @csrf
                        @if(isset($item))
                            @method('put')
                        @else
                            @method('post')
                        @endif

                        <div class="form-group form-focus">
                            <label class="focus-label">Title</label>
                            <input required type="text" name="title" placeholder="title" value="{{ $item->title ?? '' }}" class="form-control floating">
                        </div>

                        <div class="form-group form-focus">
                            <label class="focus-label">Type</label><br>
                            <select name="type" class="select enum-type">
                                @foreach(\App\Models\Property::TYPES as $type)
                                    <option {{ isset($item) && $item->type === $type ? 'selected' : '' }} value="{{ $type }}"> {{ $type }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group form-focus">
                            <label class="focus-label" for="filter-input{{$item ? $item->id : 0}}">Filter?</label><br>
                            <input type="checkbox" id="filter-input{{$item ? $item->id : 0}}" name="filter" {{ $item && $item->filter ? 'checked' : '' }} class="">
                        </div>

                        <div class="form-group form-focus">
                            <label class="focus-label" for="card-input{{$item ? $item->id : 0}}">Card?</label><br>
                            <input type="checkbox" id="card-input{{$item ? $item->id : 0}}" name="card" {{ $item && $item->card ? 'checked' : '' }} class="">
                        </div>


                        <div class="form-group form-focus enum" {{ $item && $item->type == 'enum' ? '' : 'hidden'  }}>
                            <label class="focus-label">Enum value</label>
                            <div class="input-block">

                                @if($item)
                                    @foreach($item->enums as $enum)
                                        <input required type="text" name="enums[]" placeholder="title" value="{{ $enum->value }}" class="form-control floating enum-value">
                                    @endforeach
                                @endif

                            </div>

                            <button type="button" class="btn btn-success add-enum">Add </button>
                        </div>

                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('JS')
    <script>

        let enumInput = `<input required type="text" name="enums[]" placeholder="title" value="" class="form-control floating enum-value">`;

        $(".enum-type").on('change', function (e) {
            if(this.value === 'enum') {
                $(".enum").attr("hidden", false)
                $(".enum .input-block").append(enumInput)
            } else {
                $(".enum").attr("hidden", true)
            }
        });

        $(".add-enum").click(function (e) {
            $(".enum .input-block").append(enumInput)
        })
    </script>
@endsection
