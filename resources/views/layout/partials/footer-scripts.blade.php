<script src="/assets/js/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>


<!-- Bootstrap Core JS -->
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<!-- Datetimepicker JS -->
<script src="/assets/js/moment.min.js"></script>
<!-- Full Calendar JS -->
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/assets/plugins/fullcalendar/jquery.fullcalendar.js"></script>
<!-- Sticky Sidebar JS -->
<script src="/assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
<script src="/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
<!-- Select2 JS -->
<script src="/assets/plugins/select2/js/select2.min.js"></script>
    <!-- Fancybox JS -->
    <script src="/assets/plugins/fancybox/jquery.fancybox.min.js"></script>
<!-- Dropzone JS -->
<script src="/assets/plugins/dropzone/dropzone.min.js"></script>

<!-- Bootstrap Tagsinput JS -->
<script src="/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

<!-- Profile Settings JS -->
<script src="/assets/js/profile-settings.js"></script>
<!-- Circle Progress JS -->
<script src="/assets/js/circle-progress.min.js"></script>
<!-- Slick JS -->
<script src="/assets/js/slick.js"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="">
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

<!-- Custom JS -->
<script src="/script.js"></script>

@if(Route::is(['restaurants.show']))
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6adZVdzTvBpE2yBRK8cDfsss8QXChK0I"></script>
    <script src="/assets/js/map.js"></script>
@endif
