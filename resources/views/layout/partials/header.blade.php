<?php error_reporting(0);?>
<!-- Loader -->
@if(Route::is(['map-grid','map-list']))
<div id="loader">
		<div class="loader">
			<span></span>
			<span></span>
		</div>
	</div>
	@endif
	<!-- /Loader  -->
<!-- Header -->
<header class="header ">
    <nav class="navbar navbar-expand-lg header-nav ">
        <div class="container">
            <div class="navbar-header">
                <a id="mobile_btn" href="#">
                    <span class="bar-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </a>
                <a href="/" class="navbar-brand logo hide-on-mob">
                    <img src="/assets/img/logo.png" class="img-fluid" alt="Logo">
                </a>
            </div>
            <div class="main-menu-wrapper">
                <div class="menu-header">
                    <a href="/" class="menu-logo">
                        <img src="/assets/img/logo.png" class="img-fluid" alt="Logo">
                    </a>
                    <a id="menu_close" class="menu-close" href="#">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
                <ul class="main-nav">
                    <li class="{{ Request::is('index') ? 'active' : '' }}">
                        <a href="/">Домой</a>
                    </li>

                    @php $categories = \App\Traits\Categories::getAll(); @endphp

                    @foreach ($categories as $category)
                        @if (!$category->parent_id)
                            @php $hasSubCategory = 0; @endphp

                            @foreach ($categories as $subCategory)
                                @if ($subCategory->parent_id === $category->id)
                                    @php $hasSubCategory = 1; @endphp
                                @endif
                            @endforeach

                            <li class=" {{ $hasSubCategory ? 'has-submenu' : '' }}">

                                <a href="{{ $hasSubCategory ? '/#' : '/entities/' . $category->slug }}">{{ $category->title }} <i class="{{ $hasSubCategory ? 'fas fa-chevron-down' : '' }}"> </i> </a>

                                @if ($hasSubCategory)
                                    <ul class="submenu">
                                        @foreach ($categories as $subCategory)
                                            @if ($subCategory->parent_id === $category->id)
                                                <li class=""><a href="/entities/{{ ($subCategory->slug ?: $subCategory->title) }}">{{ $subCategory->title }}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <ul class="nav header-navbar-rht">
            <li class="nav-item contact-item">
                <li class="nav-item">
                    <i class="far fa-heart"></i>
                </li>

                @if(!$user)
                    <li class="nav-item">
                        <a class="nav-link header-login" href="/login">login / Signup </a>
                    </li>
                @endif
            </li>
            @if($user)
                <li class="nav-item dropdown has-arrow logged-item">
                    <a href="/#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <span class="user-img">
                            <img class="rounded-circle" src="{{ $user && $user->avatar ? $user->avatar : '/assets/img/sample.jpg' }}" width="31" alt="Ryan Taylor">
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="user-header">
                            <div class="avatar avatar-sm">
                                <img src="{{ $user && $user->avatar ? $user->avatar : '/assets/img/sample.jpg' }}" alt="User Image" class="avatar-img rounded-circle">
                            </div>
                            <div class="user-text">
                                <h6>{{ $user->name }}</h6>
                            </div>
                        </div>
                        <a class="dropdown-item" href="/profile-settings">Профиль</a>
                        <a class="dropdown-item" href="/logout">Выход</a>
                    </div>
                </li>
            @endif
        </ul>
        </div>
    </nav>
</header>
<!-- /Header -->

<style>
    .alert {
        position: absolute;
        top: 5px;
        z-index: 99;
        left: 46%;
        min-width: 151px;
        text-align: center;
    }
</style>

@if(session()->has('message') || $errors->any())
    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast alert-toast" data-autohide="false">
        <div class="toast-header">
    {{--            <img src="..." class="rounded mr-2" alt="...">--}}
            <strong class="mr-auto">Message</strong>
    {{--            <small>11 mins ago</small>--}}
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            {{ session()->get('message') }}
            {{ implode('', $errors->all(':message')) }}
        </div>
    </div>
@endif

