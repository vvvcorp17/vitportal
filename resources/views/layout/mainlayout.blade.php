{{--@if(!isset(request()->header()['x-requested-with']))--}}
    <!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layout.partials.head')
        </head>
        <body class="">
            <div id="main-content">
{{--@endif--}}
                @include('layout.partials.header')
                <div class="container">
                    @yield('content', ['user' => $user, 'userId' => $userId])
                </div>
                @include('layout.partials.footer')

{{--@if(!isset(request()->header()['x-requested-with']))--}}
            </div>
                @include('layout.partials.footer-scripts')
        </body>
    </html>
{{--@endif--}}

@yield('JS')



