@extends('admin.mainlayout_admin')
@section('content')

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
		                       <h3 class="page-title">List of Categories</h3>
                                <a class="btn btn-sm bg-danger-light" data-toggle="modal" href="#add-category">
                                    <i class="fe fe-add"></i> Add
                                </a>

                                <div class="modal fade" id="add-category" aria-hidden="true" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add new</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-content p-2">
                                                    <form action="/admin/categories" method="post">
                                                        @csrf

                                                        <div class="form-group form-focus">
                                                            <input type="text" name="title" placeholder="title" class="form-control floating">
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="select" name="parent_id">
                                                                <option selected value>Select Parent Category</option>

                                                                @foreach(\App\Traits\Categories::getAll() as $category)
                                                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group form-focus">
                                                            <input type="text" name="slug" placeholder="slug" class="form-control floating">
                                                        </div>

                                                        <div class="custom-control custom-checkbox">
                                                            <label>Properties</label>

{{--                                                            @foreach($props as $prop)--}}
{{--                                                                <div>--}}
{{--                                                                    <input type="checkbox" name="property[]" value="{{ $prop->id }}" class="custom-control-input" id="property{{ $prop->id }}">--}}
{{--                                                                    <label class="custom-control-label" for="property{{ $prop->id }}">{{ $prop->title }}({{ $prop->type }})</label>--}}
{{--                                                                </div>--}}
{{--                                                            @endforeach--}}
                                                            <br>
                                                        </div>

                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-success">Save </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            {{ implode('', $errors->all(':message')) }}
                        </div>
                    @endif

					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>#</th>
													<th>Parent</th>
													<th>Slug</th>
													<th>Title</th>
													<th>Description</th>
													<th>Created At</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>

                                                @foreach($items as $item)
                                                    <tr data-id="$item->id">
                                                        <td>{{ $item->id }}</td>
                                                        <td>{{ $item->parent_id }}</td>
                                                        <td>{{ $item->slug }}</td>
                                                        <td>{{ $item->title }}</td>
                                                        <td>{{ $item->description }}</td>
                                                        <td>{{ $item->created_at }}</td>

                                                        <td class="">
                                                            <div class="actions">
                                                                <a class="btn btn-sm bg-success-light" data-toggle="modal" href="#edit_modal{{ $item->id }}">
                                                                    <i class="fe fe-pencil"></i> Edit
                                                                </a>

                                                                <div class="modal fade" id="edit_modal{{ $item->id }}" aria-hidden="true" role="dialog">
                                                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title">Edit</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="form-content p-2">
                                                                                    <form action="/admin/categories/{{ $item->id }}" method="post">
                                                                                        @csrf
                                                                                        @method('put')

                                                                                        <div class="form-group form-focus">
                                                                                            <label class="focus-label">Title</label>
                                                                                            <input type="text" name="title" placeholder="title" value="{{ $item->title }}" class="form-control floating">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <select class="select" name="parent_id">
                                                                                                <option disabled selected value>Select Parent Category</option>

                                                                                                @foreach(\App\Traits\Categories::getAll() as $category)
                                                                                                    <option {{ $item->parent_id === $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->title }}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>

                                                                                        <label>Properties</label>
                                                                                        <div class="custom-control custom-checkbox">

                                                                                            @foreach($props as $prop)
                                                                                                @php $select = false; @endphp

                                                                                                @foreach($item->properties as $ip)
                                                                                                    @if($prop->id === $ip->id)
                                                                                                        @php $select = true; @endphp
                                                                                                    @endif
                                                                                                @endforeach

                                                                                                <div>
                                                                                                    <input
                                                                                                        type="checkbox"
                                                                                                        name="properties[]"
                                                                                                        {{ $select ? 'checked' : '' }}
                                                                                                        value="{{ $prop->id }}"
                                                                                                        class="custom-control-input"
                                                                                                        id="property{{ $item->id }}{{ $prop->id }}"
                                                                                                    >
                                                                                                    <label class="custom-control-label" for="property{{ $item->id }}{{ $prop->id }}">{{ $prop->title }}({{ $prop->type }})</label>
                                                                                                </div>
                                                                                            @endforeach
                                                                                            <br>
                                                                                        </div>

                                                                                        <div class="form-group form-focus">
                                                                                            <label >Slug</label>
                                                                                            <input type="text" name="slug" value="{{ $item->slug }}" placeholder="slug" class="form-control ">
                                                                                        </div>
                                                                                        <div class="form-group form-focus">
                                                                                            <label class="focus-label">Description</label>
                                                                                            <input type="text" name="description" value="{{ $item->description }}" placeholder="description" class="form-control ">
                                                                                        </div>

                                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-success">Save </button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <a data-toggle="modal" href="#delete_modal{{ $item->id }}" class="btn btn-sm bg-danger-light">
                                                                    <i class="fe fe-trash"></i> Delete
                                                                </a>

                                                                <div class="modal fade" id="delete_modal{{ $item->id }}" aria-hidden="true" role="dialog">
                                                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                                                        <div class="modal-content">
                                                                            <div class="modal-body">
                                                                                <div class="form-content p-2">
                                                                                    <h4 class="modal-title">Delete</h4>
                                                                                    <p class="mb-4">Are you sure want to delete the "{{ $item->title }}" category?</p>
                                                                                    <form action="/admin/categories/{{ $item->id }}" method="post">
                                                                                        @method('delete')
                                                                                        @csrf
                                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary">Delete </button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
@endsection
