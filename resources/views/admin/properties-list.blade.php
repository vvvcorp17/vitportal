@extends('admin.mainlayout_admin')
@section('content')

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
		                       <h3 class="page-title">List of properties</h3>
                                <a class="btn btn-sm bg-danger-light" data-toggle="modal" href="#manage_property_modal">
                                    <i class="fe fe-add"></i> Add
                                </a>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            {{ implode('', $errors->all(':message')) }}
                        </div>
                    @endif

					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>#</th>
													<th>Title</th>
													<th>Created At</th>
													<th>Updated At</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>

                                                @foreach($items as $item)
                                                    <tr data-id="$item->id">
                                                        <td>{{ $item->id }}</td>
                                                        <td>{{ $item->title }}( {{ $item->type }} )</td>
                                                        <td>{{ $item->created_at }}</td>
                                                        <td>{{ $item->updated_at }}</td>

                                                        <td class="">
                                                            <div class="actions">
                                                                <a class="btn btn-sm bg-success-light" data-toggle="modal" href="#manage_property_modal{{ $item->id }}">
                                                                    <i class="fe fe-pencil"></i> Edit
                                                                </a>

                                                                @include('layout.partials.property-manage', ['item' => $item])

                                                                <a data-toggle="modal" href="#delete_modal{{ $item->id }}" class="btn btn-sm bg-danger-light">
                                                                    <i class="fe fe-trash"></i> Delete
                                                                </a>

                                                                <div class="modal fade" id="delete_modal{{ $item->id }}" aria-hidden="true" role="dialog">
                                                                    <div class="modal-dialog modal-dialog-centered" role="document" >
                                                                        <div class="modal-content">
                                                                            <div class="modal-body">
                                                                                <div class="form-content p-2">
                                                                                    <h4 class="modal-title">Delete</h4>
                                                                                    <p class="mb-4">Are you sure want to delete the "{{ $item->title }}" property?</p>
                                                                                    <form action="/admin/properties/{{ $item->id }}" method="post">
                                                                                        @method('delete')
                                                                                        @csrf
                                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary">Delete </button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

            @include('layout.partials.property-manage', ['item' => null])
@endsection

