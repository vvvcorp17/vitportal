<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.head_admin')
  </head>

  <body>
  @if(!Route::is(['login','register','forgot-password','lock-screen','error-404','error-500']))
    @include('admin.header_admin')
    @include('admin.nav_admin')
  @endif
 @yield('content')
 @include('layout.partials.footer_admin-scripts')
 @yield('JS')

  </body>
</html>
