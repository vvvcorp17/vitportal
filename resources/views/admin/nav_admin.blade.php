<!-- Sidebar -->
<div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="menu-title">
								<span>Main</span>
							</li>
							<li>
								<a href="/admin"><i class="fe fe-home"></i> <span>Dashboard</span></a>
							</li>

							<li>
								<a href="/admin/users"><i class="fe fe-user"></i> <span>Users</span></a>
							</li>

							<li>
								<a href="/admin/categories"><i class="fe fe-document"></i> <span>Categories</span></a>
							</li>

							<li>
								<a href="/admin/properties"><i class="fe fe-user"></i> <span>Properties</span></a>
							</li>

						</ul>
					</div>
                </div>
            </div>
			<!-- /Sidebar -->
