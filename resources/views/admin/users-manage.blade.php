@extends('admin.mainlayout_admin')
@section('content')

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Users</h3>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>email</th>
													<th>Phone</th>
													<th>Country</th>
													<th>Location</th>
													<th>Created At</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>

                                                @foreach($items as $item)
                                                    <tr data-id="$item->id">
                                                        <td>
                                                            <h2 class="table-avatar">
                                                                <a href="original/profile" class="avatar avatar-sm mr-2">
                                                                    <img class="avatar-img rounded-circle" src="{{ $item->avatar ?: '/../assets_admin/img/sample.jpg' }}" alt="User Image">
                                                                </a>
                                                                <a href="original/profile">{{ $item->name }}</a>
                                                            </h2>
                                                        </td>
                                                        <td>{{ $item->name }}</td>
                                                        <td>{{ $item->email }}</td>
                                                        <td>{{ $item->phone }}</td>
                                                        <td>{{ $item->country }}</td>
                                                        <td>{{ $item->location }}</td>
                                                        <td>{{ $item->created_at }}</td>

                                                        <td class="text-right">
                                                            <div class="actions">
                                                                <a class="btn btn-sm bg-success-light" data-toggle="modal" href="#edit_specialities_details">
                                                                    <i class="fe fe-pencil"></i> Edit
                                                                </a>
                                                                <a data-toggle="modal" href="#delete_modal{{ $item->id }}" class="btn btn-sm bg-danger-light">
                                                                    <i class="fe fe-trash"></i> Delete
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>


													<div class="modal fade" id="delete_modal{{ $item->id }}" aria-hidden="true" role="dialog">
														<div class="modal-dialog modal-dialog-centered" role="document" >
															<div class="modal-content">
																<div class="modal-body">
																	<div class="form-content p-2">
																		<h4 class="modal-title">Delete</h4>
																		<p class="mb-4">Are you sure want to delete the "{{ $item->title }}" property?</p>
																		<form action="/admin/users/{{ $item->id }}" method="post">
																			@method('delete')
																			@csrf
																			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
																			<button type="submit" class="btn btn-primary">Delete </button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
                                                @endforeach

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
@endsection
