<?php $page="profile-settings";?>

@extends('layout.mainlayout')
@section('content')
    <!-- Breadcrumb -->
    <div class="breadcrumb-bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Настройки профиля</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title">Администрирование</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- /Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
                    <!-- Profile Sidebar -->
                @include('layout.partials.profile-sidebar')
                <!-- /Profile Sidebar -->
                </div>

                <div class="col-md-7 col-lg-8 col-xl-9 row">
                    <div class="row">
                        @foreach($entities as $entity)
                            <div class="col-md-4 col-lg-4">
                                @include('entity.entity', ['manage' => 1])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
