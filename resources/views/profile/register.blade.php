<?php $page="register1";?>
@extends('layout.mainlayout')
@section('content')
<!-- Page Content -->
<div class="content account-page" style="padding: 50px 0;">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-8 offset-md-2">

							<!-- Register Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="https://img.icons8.com/carbon-copy/2x/login-rounded-right.png" class="img-fluid" alt="Weddi Register">
									</div>
									<div class="col-md-12 col-lg-6 login-right">
																				<!-- Register Form -->
										<form action="/register" method="post" onsuccess="changeURL('/profile-settings')">
                                            @csrf

											<div class="login-header">
												<h3 style="float: left">Register</h3>
												<div style="float: right" class="custom-control custom-checkbox">
													<input type="checkbox" name="business" value="1" class="custom-control-input" id="businessCheck">
													<label class="custom-control-label" for="businessCheck">Бизнесс аккаунт</label>
												</div>
												<br>
											</div>

                                            <div class="form-group card-label form-focus">
												<label>Name</label>
												<input type="text" required name="name" class="form-control floating">
                                            </div>
                                            <div class="form-group card-label form-focus">
												<label>Email</label>
												<input type="email" required name="email" class="form-control floating">
											</div>

                                            <div class="form-group card-label form-focus">
												<label>Password</label>
												<input type="password" required name="password" class="form-control floating">
                                            </div>

                                            <div class="form-group card-label form-focus">
												<label>Password confirmation</label>
												<input type="password" required name="password_confirmation" class="form-control floating">
                                            </div>

											<div class="text-right">
												<a class="forgot-link" href="/login">Уже есть аккаунт?</a>
											</div>
											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Signup</button>
											<div class="login-or">
{{--												<span class="or-line"></span>--}}
{{--												<span class="span-or">or</span>--}}
											</div>

{{--											<div class="row form-row social-login">--}}
{{--												<div class="col-6">--}}
{{--													<a href="#" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f mr-1"></i> Login</a>--}}
{{--												</div>--}}
{{--												<div class="col-6">--}}
{{--													<a href="#" class="btn btn-google btn-block"><i class="fab fa-google mr-1"></i> Login</a>--}}
{{--												</div>--}}
{{--											</div>--}}
										</form>
										<!-- /Register Form -->

									</div>
								</div>
							</div>
							<!-- /Register Content -->

						</div>
					</div>

				</div>

			</div>
@endsection
