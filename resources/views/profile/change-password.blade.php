<?php $page="change-password";?>
@extends('layout.mainlayout')
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Change Password</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Change Password</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">

							<!-- Profile Sidebar -->
                            @include('layout.partials.profile-sidebar')
                            <!-- /Profile Sidebar -->

						</div>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-body">
									<div class="row">
										<div class="col-md-12 col-lg-6">

											<!-- Change Password Form -->
											<form method="post" action="/change-password">
                                                @csrf
												<div class="form-group card-label">
													<label>Old Password</label>
													<input required type="password" name="old-password" value="{{ old('old-password') }}" class="form-control">
												</div>
												<div class="form-group card-label">
													<label>New Password</label>
													<input required type="password" name="password" value="{{ old('password') }}" class="form-control">
												</div>
												<div class="form-group card-label">
													<label>Confirm Password</label>
													<input required type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control">
												</div>
												<div class="submit-section">
													<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
												</div>
											</form>

                                            <br>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
@endsection
