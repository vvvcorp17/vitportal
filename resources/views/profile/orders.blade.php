<?php $page="profile-settings";?>

@extends('layout.mainlayout')
@section('content')
    <!-- Breadcrumb -->
    <div class="breadcrumb-bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Настройки профиля</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title">Мой Бизнес</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- /Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
                    <!-- Profile Sidebar -->
                @include('layout.partials.profile-sidebar')
                <!-- /Profile Sidebar -->
                </div>

                <div class="col-md-7 col-lg-8 col-xl-9">
                    @if(count($myOrders))
                        <div class="card card-table">
                            <div class="card-body">
                                <h2>Мои заказы</h2>
                                <!-- Invoice Table -->
                                <div class="table-responsive">
                                    <table class="table table-hover table-center mb-0">
                                        <thead>
                                        <tr>
                                            <th>Номер заказа</th>
                                            <th>Ресторан</th>
                                            <th>Дата/время</th>
                                            <th>Примечание</th>
                                            <th>Статус</th>
                                            <th>Создан</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($myOrders as $order)
                                            <tr>
                                                <td style="text-align: center;">
                                                    #{{ $order->id }}
                                                    @if($order->status !== 'declined')
                                                        <div class="appointment-action">
                                                            <label class="btn btn-sm bg-danger-light" for="decline{{ $order->id }}">
                                                                Decline
                                                            </label>
                                                        </div>
                                                        <form action="/orders/{{ $order->id }}" method="post">
                                                            @csrf
                                                            @method('put')
                                                            <input type="hidden" name="status" value="declined">
                                                            <input hidden type="submit" id="decline{{ $order->id }}">
                                                        </form>
                                                    @endif
                                                </td>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="/halls/{{ $order->entity->id }}" class="avatar avatar-sm mr-2">
                                                            <img class="avatar-img" src="{{ $order->entity->getMainImage() }}" alt="User Image">
                                                        </a>
                                                        <a href="#">{{ $order->entity->title }}</a>
                                                    </h2>
                                                </td>
                                                <td>{{ $order->to_date }}</td>
                                                <td>{{ $order->note }}</td>
                                                <td>
                                                    <button type="button"
                                                            class="btn btn-rounded btn-outline-{{ $order->status === 'accepted' ? 'success' : ($order->status === 'declined' ? 'danger' : 'dark')   }}"
                                                    >
                                                        {{ ucfirst($order->status) }}
                                                    </button>
                                                </td>
                                                <td>{{ $order->created_at }}</td>
                                                <td>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($user->business)
                        <div class="card card-table">
                            <div class="card-body">
                                <h2>Клиентские заказы</h2>
                                <!-- Invoice Table -->
                                <div class="table-responsive">
                                    <table class="table table-hover table-center mb-0">
                                        <thead>
                                        <tr>
                                            <th>Номер заказа</th>
                                            <th>Пользователь</th>
                                            <th>Ресторан</th>
                                            <th>Дата/время</th>
                                            <th>Примечание</th>
                                            <th>Статус</th>
                                            <th>Создан</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($clientOrders as $order)
                                            <tr>
                                                <td style="text-align: center;">
                                                    #{{ $order->id }}
                                                    <div class="appointment-action">
                                                        @if($order->status !== 'accepted')
                                                            <label class="btn btn-sm bg-success-light " for="accept{{ $order->id }}">
                                                                Accept
                                                            </label>
                                                        @endif
                                                        @if($order->status !== 'declined')
                                                            <label class="btn btn-sm bg-danger-light" for="decline{{ $order->id }}">
                                                                Decline
                                                            </label>
                                                        @endif
                                                        @if($user && $user->role === 'admin' )
                                                            <label class="btn btn-sm bg-danger-light" for="delete{{ $order->id }}">
                                                                Delete
                                                            </label>
                                                        @endif
                                                    </div>

                                                    <form onsubmit="return confirm('Вы уверены?')" action="/orders/{{ $order->id }}" method="post">
                                                        @csrf
                                                        @method('put')
                                                        <input type="hidden" name="status" value="accepted">
                                                        <input hidden type="submit" id="accept{{ $order->id }}">
                                                    </form>

                                                    <form onsubmit="return confirm('Вы уверены?')" action="/orders/{{ $order->id }}" method="post">
                                                        @csrf
                                                        @method('put')
                                                        <input type="hidden" name="status" value="declined">
                                                        <input hidden type="submit" id="decline{{ $order->id }}">
                                                    </form>

                                                    <form onsubmit="return confirm('Вы уверены?')" action="/orders/{{ $order->id }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <input hidden type="submit" id="delete{{ $order->id }}">
                                                    </form>

                                                </td>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="#" class="avatar avatar-sm mr-2">
                                                            <img class="avatar-img rounded-circle" src="{{ $order->user->avatar ?? '/assets/img/sample.jpg' }}" alt="User Image">
                                                        </a>
                                                        <div >{{ $order->user->name }}
                                                            <span>{{ $order->user->email }}</span>
                                                            <span><a id="call" href="tel:{{ $order->user->phone }}" data-action="call" class="moto-link">{{ $order->user->phone }}</a></span>
                                                        </div>
                                                    </h2>
                                                </td>
                                                <td>
                                                    <h2 class="table-avatar">
                                                        <a href="/halls/{{ $order->entity->id }}" class="avatar avatar-sm mr-2">
                                                            <img class="avatar-img rounded-circle" src="{{ $order->entity->getMainImage() }}" alt="User Image">
                                                        </a>
                                                        <a href="#">{{ $order->entity->title }}</a>
                                                    </h2>
                                                </td>
                                                <td>{{ $order->to_date }}</td>
                                                <td>{{ $order->note }}</td>
                                                <td><button type="button"
                                                            class="btn btn-rounded btn-outline-{{ $order->status === 'accepted' ? 'success' : ($order->status === 'declined' ? 'danger' : 'dark')   }}"
                                                    >
                                                        {{ ucfirst($order->status) }}
                                                    </button>
                                                </td>
                                                <td>{{ $order->created_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
