<?php $page="profile-settings";?>

@extends('layout.mainlayout')
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Настройки профиля</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Настройки профиля</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
                            <!-- Profile Sidebar -->
                            @include('layout.partials.profile-sidebar')
                            <!-- /Profile Sidebar -->
                        </div>


						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-body">

									<!-- Настройки профиля Form -->
									<form method="post" action="/profile-settings" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row form-row">
											<div class="col-12 col-md-12">
												<div class="form-group">
													<div class="change-avatar">
														<div class="profile-img">
															<img src="{{ $user->avatar ?: '/assets/img/sample.jpg' }}" alt="User Image">
														</div>
														<div class="upload-img">
															<div class="change-photo-btn">
																<span><i class="fa fa-upload"></i> Upload Photo</span>
																<input type="file"  accept=".jpg, .jpeg, .png" name="avatar" class="upload">
															</div>
															<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
														</div>
													</div>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group card-label">
													<label>Name</label>
													<input type="text" required name="name" class="form-control" value="{{ $user->name }}">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group card-label">
													<label>Email</label>
													<input type="email" required name="email" class="form-control" value="{{ $user->email }}">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group card-label">
													<label>Mobile</label>
													<input type="number" id="phone" required name="phone" placeholder="XXX XXX XX XXX" data-inputmask-mask="(999) 999-9999" value="{{ $user->phone }}" class="form-control">
												</div>
											</div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group card-label">
                                                    <label>Country</label>
                                                    <select required name="country" class="form-control select">
                                                        <option {{ $user->country === 'Moldova' ? 'selected' : '' }} value="Moldova">Moldova</option>
                                                        <option {{ $user->country === 'Ukraina' ? 'selected' : '' }} value="Ukraina">Ukraina</option>
                                                    </select>
                                                </div>
                                            </div>
											<div class="col-12 col-md-6">
												<div class="form-group card-label">
													<label>Location</label>
													<input required type="text" name="location" class="form-control" value="{{ $user->location }}">
												</div>
											</div>
										</div>

										<div class="submit-section">
											<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
										</div>
									</form>
									<!-- /Настройки профиля Form -->

								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
@endsection
