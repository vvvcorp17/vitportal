<?php $page="forgot-pswd";?>
@extends('layout.mainlayout')
@section('content')
<!-- Page Content -->
<div class="content account-page" style="padding: 50px 0;">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-8 offset-md-2">

							<!-- Account Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="https://img.icons8.com/carbon-copy/2x/login-rounded-right.png" class="img-fluid" alt="Login Banner">
									</div>
									<div class="col-md-12 col-lg-6 login-right">
										<div class="login-header">
											<h3>Forgot Password?</h3>
											<p class="small text-muted">Enter your email to get a password reset link</p>
										</div>

										<!-- Forgot Password Form -->
										<form action="/forgot-password" method="post">
                                            @csrf
                                            <div class="form-group card-label form-focus">
												<label>Email</label>
												<input type="email" name="email" class="form-control floating">
											</div>

											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Reset Password</button>
										</form>
										<!-- /Forgot Password Form -->

									</div>
								</div>
							</div>
							<!-- /Account Content -->

						</div>
					</div>

				</div>

			</div>
@endsection
