@if ($paginator->hasPages())
    <div class="row">
        <div class="col-md-12">
            <div class="blog-pagination">
                <nav>
                    <ul class="pagination justify-content-center">

                        @if (!$paginator->onFirstPage())
                            <li class="page-item ">
                                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" tabindex="-1"><i class="fas fa-angle-double-left"></i></a>
                            </li>
                        @endif

                        @foreach ($elements as $element)
                            {{-- "Three Dots" Separator --}}
                            @if (is_string($element))
                                    <li class="page-item">
                                        <a class="page-link" >{{ $element }}</a>
                                    </li>
                            @endif

                            {{-- Array Of Links --}}
                            @if (is_array($element))
                                @foreach ($element as $page => $url)
                                    <li class="page-item {{ $page == $paginator->currentPage() ? 'active' : '' }} ">
                                        <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach

                        @if ($paginator->hasMorePages())
                            <li class="page-item">
                                <a class="page-link" href="{{ $paginator->nextPageUrl() }}"><i class="fas fa-angle-double-right"></i></a>
                            </li>
                        @endif
                    </ul>
                    <p class="text-sm text-gray-700 leading-5">
                        {!! __('Showing') !!}
                        <span class="font-medium">{{ $paginator->firstItem() }}</span>
                        {!! __('to') !!}
                        <span class="font-medium">{{ $paginator->lastItem() }}</span>
                        {!! __('of') !!}
                        <span class="font-medium">{{ $paginator->total() }}</span>
                        {!! __('results') !!}
                    </p>
                </nav>
            </div>
        </div>
    </div>
@endif
