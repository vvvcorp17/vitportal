<div class="modal fade" id="manage-entity{{ $entity->id ?? '' }}" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">

                @if(!isset($category))
                    @php $category = $entity->category; @endphp
                @endif

                <h5 class="modal-title">{{ $category->title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="step-form" method="post" action="/entities/{{ $category->slug }}/{{ $entity->id ?? '' }}" enctype="multipart/form-data" onsubmit="$('#manage-entity{{ $entity->id ?? '' }}').modal('toggle')">
                    @csrf
                    @if(isset($entity))
                        @method('put')
                    @endif

                    <input type="hidden" name="entity_id" value="{{ $entity->id ?? '' }}">

                    <div class="step-form">

                        <div class="tab active">Название:
                            <div class="col-12 col-sm-12">
                                <div class="form-group card-label">
                                    <label>Название</label>
                                    <input type="text" required class="form-control" name="title" value="{{ $entity->title ?? '' }}">
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-group card-label">
                                    <label>Короткое описание (до 200 символов)</label>
                                    <input type="text" class="form-control" name="short_description" value="{{ $entity->short_description ?? '' }}">
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-group card-label">
                                    <label>Описание</label>
                                    <textarea rows="10"  class="form-control" name="description"> {{ $entity->description ?? '' }} </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="tab">Атрибуты:
                            <div class="col-12 col-sm-12">
                                <div class="form-group card-label">
                                    <label>Цена</label>
                                    <input type="number" required class="form-control" name="price" value="{{ $entity->price ?? '' }}">
                                </div>
                            </div>

                            @foreach($category->props as $prop)
                                <div class="col-12 col-sm-12">

                                    <div class="form-group card-label">
                                        <label>{{ $prop->title }}</label>
                                        @if($prop->type == 'enum')
                                            <select name="properties[{{ $prop->id }}][]" class="custom-select enum-type" multiple>
                                                @foreach($prop->enums as $enum)
                                                    <option {{ $entity && isset($entity->properties[$prop->id]) && in_array($enum->value, (array)$entity->properties[$prop->id]) ? 'selected' : '' }} value="{{ $enum->value }}">{{ $enum->value }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="{{ $prop->type }}" class="form-control" name="properties[{{ $prop->id }}]"value="{{ $entity->properties[$prop->id] ?? ''}}">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="tab">Изображения:
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <div class="image-preview-block">
                                        <img class="img-preview" width="100px" src="{{ $entity ? $entity->getMainImage() : '/assets/img/sample.jpg' }}"  alt="">
                                        <label for="file-manage{{ $entity->id ?? '' }}"  class="btn btn-primary">Main image</label>
                                        <input id="file-manage{{ $entity->id ?? '' }}" max="1mb" name="mainImage" class="mainImage" accept="image/*" hidden type="file">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <div class="image-preview-block">
                                        @if(isset($entity->image))
                                            <img class="img-preview" width="100px" src="{{ $entity->image }}" alt="">
                                        @else
                                            <label>Изображения</label>
                                        @endif
                                        <label for="files-manage{{ $entity->id ?? '' }}"  class="btn btn-primary">Images</label>
                                    </div>
                                    <input id="files-manage{{ $entity->id ?? '' }}" max="1mb" class="uploadImages" name="images[]" accept="image/*" hidden type="file" multiple>

                                    <div class="row">
                                        @if($entity)
                                            @foreach($entity->getImages() as $image)
                                                <div class="col-3">

                                                    @if($entity->user_id === $userId)
                                                        <i
                                                                class="fas fa-trash action entity-image-trash"
                                                                style=" position: absolute;left: 50px;top: 50px;"
                                                                data-id="{{ $entity->id ?? '' }}"
                                                                data-image="{{ $image }}"
                                                                data-token="{{ csrf_token() }}"
                                                        >
                                                        </i>
                                                    @endif

                                                    <img width="100px" src="/{{ $image }}" alt="">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow:auto;">
                            <div style="float:right;">
                                <button type="button" id="prevBtn" onclick="nextPrev(this, -1)" style="display: none" >Назад</button>
                                <button type="button" id="nextBtn" onclick="nextPrev(this, 1)">Вперед</button>
                                <button type="submit" id="sendForm" style="display: none" >Отправить</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function nextPrev(el, direction)
    {
        let stepForm = $(el).closest(".step-form");
        let activeTab = stepForm.children(".tab.active");

        if (!validateStepForm(activeTab)) {
            return;
        }

        activeTab.toggleClass("active");

        if (direction > 0) {
            activeTab.next(".tab").toggleClass("active");
        } else {
            activeTab.prev(".tab").toggleClass("active");
        }

        $("#prevBtn", stepForm).show();
        $("#nextBtn", stepForm).show();
        $("#sendForm", stepForm).hide();

        if ($(".tab", stepForm).first().hasClass('active')) {
            $("#prevBtn", stepForm).toggle();
        }
        if ($(".tab", stepForm).last().hasClass('active')) {
            $("#nextBtn", stepForm).toggle();
            $("#sendForm", stepForm).toggle();
        }
    }

    function validateStepForm(activeTab) {
        let valid = true;
        $("input:required", activeTab).each(function (e) {
            if (!$(this).val()) {
                $(this).addClass("invalid");
                valid = false;
            }
        });

        return valid;
    }
</script>
