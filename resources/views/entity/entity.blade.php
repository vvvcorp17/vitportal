<div class="profile-widget " style="width: 100%; display: inline-block; height: max-content;">
    <div class="doc-img">
        <a href="/entities/{{ $entity->category->slug }}/{{ $entity->id }}" tabindex="0">
            <img class="img-fluid" alt="User Image" src="{{ $entity->getMainImage() }}">
        </a>

        <div class="dropdown dropdown-action" style="position: absolute;top: 0px;right: 0px;">
            @if($userId === $entity->user_id && isset($manage))
                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="material-icons">settings</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" data-toggle="modal" href="#manage-entity{{ $entity->id }}">
                        <i class="fa fa-pen-alt m-r-5"></i>
                        Edit
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="fa fa-trash m-r-5"></i>
                        <label for="delete-entity{{ $entity->id }}">
                            Delete
                        </label>
                    </a>
                </div>
            @endif
        </div>
    </div>
    <div class="pro-content">
        <h3 class="title">
            <a href="/entities/{{ $entity->category->slug }}/{{ $entity->id }}" tabindex="0">{{ $entity->title }}</a>
            <i class="far fa-heart" style="float: right"></i>
        </h3>
        <p class="speciality">{{ $entity->short_description }}</p>
        <p>
            <a href="">{{ $entity->category->title }}</a>
        </p>
        <div class="rating">
            <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 1 ? 'filled' : '' }}"></i>
            <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 2 ? 'filled' : '' }}"></i>
            <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 3 ? 'filled' : '' }}"></i>
            <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 4 ? 'filled' : '' }}"></i>
            <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 5 ? 'filled' : '' }}"></i>
            <span class="d-inline-block average-rating">({{ $entity->reviews->count('rate') }})</span>
        </div>
        <ul class="available-info">
            <li>
                <i class="far fa-money-bill-alt"></i>{{ $entity->price }} MDL
            </li>

            @foreach($entity->category->props as $prop)
                @if(!isset($entity->properties[$prop->id]) || !$prop->card) @continue; @endif
                @php
                    $value = $entity->properties[$prop->id];
                    $value = is_array($value) ? implode(', ', $value) : $value;
                @endphp
                <li>
                    <i class="fas fa-map-marker-alt"></i> {{ $prop->title }}: {{ $value }}
                </li>
            @endforeach
        </ul>
    </div>
</div>

@if($userId === $entity->user_id && isset($manage))
    <form action="/entities/{{ $entity->category->slug }}/{{ $entity->id }}" method="post" onsubmit="return confirm('Вы уверены?');" >
        @csrf
        @method('delete')
        <input type="submit" hidden id="delete-entity{{ $entity->id }}">
    </form>
    @include('entity.entity-manage')
@endif

