<?php $page="doctor-profile";?>
@extends('layout.mainlayout')
@section('content')
<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Домой</a></li>
                        <li class="breadcrumb-item"><a href="/entities/{{ $category->slug }}">{{ $category->title }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $entity->title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content">
    <div class="container" style="max-width: 1440px">

        <!-- Doctor Widget -->
        <div class="card">
            <div class="card-body">
                <div class="doctor-widget">
                    <div class="doc-info-left">
                        <div class="doctor-img">
                            <img width="650px" class="img-fluid" alt="User Image" src="{{ $entity->getMainImage() }}"
                                 onerror="this.src='/assets/img/sample.jpg';"
                            >
                        </div>
                        <div class="doc-info-cont" style="margin-left: 30px;">
                            <h2 class="doc-name">{{ $entity->title }}</h2>
                            <p class="doc-speciality">{{ $entity->short_description }}</p>

                            <div class="rating">
                                <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 1 ? 'filled' : '' }}"></i>
                                <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 2 ? 'filled' : '' }}"></i>
                                <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 3 ? 'filled' : '' }}"></i>
                                <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 4 ? 'filled' : '' }}"></i>
                                <i class="fas fa-star {{ $entity->reviews->avg('rate') >= 5 ? 'filled' : '' }}"></i>
                                <span class="d-inline-block average-rating">({{ count($entity->reviews) }})</span>
                            </div>
                            <div class="clinic-details">
                                <p><i class="far "></i> Цена : {{ $entity->price }} MDL</p>

                                @foreach($category->props as $prop)
                                    <p><i class="far "></i>
                                        @if(is_array($entity->properties[$prop->id]))
                                            {{ $prop->title }} : {{ @implode(', ', $entity->properties[$prop->id]) }}
                                        @else
                                            {{ $prop->title }} : {{ $entity->properties[$prop->id] }}
                                        @endif
                                    </p>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="doc-info-right">
                        <div class="clinic-booking">
                            <a class="apt-btn" style="cursor: pointer" data-toggle="modal" data-target="#book">Забронировать</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clinic-details">
                <ul class="clinic-gallery">
                    @foreach($entity->getImages() as $image)
                        <li>
                            <a href="/{{ $image }}"
                               data-fancybox="gallery">
                                <img src="/{{ $image }}" alt="Feature">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /Doctor Widget -->

        <div id="book" class="modal custom-modal fade" role="dialog" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Забронировать</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/orders" method="post">
                            @csrf
                            <input type="hidden" name="entity_id" value="{{ $entity->id }}">
                            <div class="form-group card-label col-sm-8">
                                <label>Дата/Время </label>
                                <div class="cal-icon">
                                    <input required class="form-control datetimepicker" autocomplete="off" name="to_date" type="text">
                                </div>
                            </div>
                            <div class="form-group card-label col-sm-12">
                                <label>примечание</label>
                                <textarea required class="form-control" name="note" type="text"> </textarea>
                            </div>
                            <div class="submit-section">
                                <button class="btn btn-primary submit-btn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Doctor Details Tab -->
        <div class="card">
            <div class="card-body pt-0">

                <!-- Tab Content -->
                <div class=" pt-0">

                    <!-- Overview Content -->
                    <div id="doc_overview" class=" pt-1 ">{!! nl2br(e($entity->description)) !!}</div>
                    <!-- /Overview Content -->

                    <!-- Reviews Content -->
                    <div id="reviews" class="pt-5 ">
                        <h3>Отзывы</h3>

                        <!-- Review Listing -->
                        <div class="widget review-listing">
                            <ul class="comments-list">
                                @foreach($reviews as $review)
                                <li>
                                    <div class="comment">
                                        <img class="avatar avatar-sm rounded-circle" alt="User Image" src="{{ $review->user->avatar ?? '/assets/img/sample.jpg' }}">
                                        <div class="comment-body">
                                            <div class="meta-data">
                                                <span class="comment-author">{{ $review->user->name }}</span>
                                                <span class="comment-date">{{ $review->created_at }}</span>
                                                <div class="review-count rating">
                                                    <i class="fas fa-star {{ $review->rate >= 1 ? 'filled' : '' }}"></i>
                                                    <i class="fas fa-star {{ $review->rate >= 2 ? 'filled' : '' }}"></i>
                                                    <i class="fas fa-star {{ $review->rate >= 3 ? 'filled' : '' }}"></i>
                                                    <i class="fas fa-star {{ $review->rate >= 4 ? 'filled' : '' }}"></i>
                                                    <i class="fas fa-star {{ $review->rate >= 5 ? 'filled' : '' }}"></i>
                                                </div>
                                            </div>

                                            <p class="comment-content">
                                                {{ $review->review }}
                                            </p>

                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>

                            <!-- Show All -->
{{--                            <div class="all-feedback text-center">--}}
{{--                                <a href="#" class="btn btn-primary btn-sm">--}}
{{--                                    Show all feedback <strong>(167)</strong>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                            <!-- /Show All -->

                        </div>
                        <!-- /Review Listing -->

                        <!-- Write Review -->
                        <div class="write-review">
                            <h4>Написать отзыв о  <strong>{{ $entity->title }}</strong></h4>

                            @if($user)

                            <!-- Write Review Form -->
                            <form method="post" action="/reviews">
                                @csrf
                                <input type="hidden" name="entity_id" value="{{ $entity->id }}">
                                <div class="form-group">
                                    <label>Оценка</label>
                                    <div class="star-rating">
                                        <input id="star-5" type="radio" name="rate" value="5">
                                        <label for="star-5" title="5 stars">
                                            <i class="active fa fa-star"></i>
                                        </label>
                                        <input id="star-4" type="radio" name="rate" value="4">
                                        <label for="star-4" title="4 stars">
                                            <i class="active fa fa-star"></i>
                                        </label>
                                        <input id="star-3" type="radio" name="rate" value="3">
                                        <label for="star-3" title="3 stars">
                                            <i class="active fa fa-star"></i>
                                        </label>
                                        <input id="star-2" type="radio" name="rate" value="2">
                                        <label for="star-2" title="2 stars">
                                            <i class="active fa fa-star"></i>
                                        </label>
                                        <input id="star-1" type="radio" name="rate" value="1">
                                        <label for="star-1" title="1 star">
                                            <i class="active fa fa-star"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Отзыв</label>
                                    <textarea required id="review_desc" name="review" maxlength="500" class="form-control"></textarea>
                                </div>
                                <div class="submit-section">
                                    <button type="submit" class="btn btn-primary submit-btn">Add Review</button>
                                </div>
                            </form>
                            <!-- /Write Review Form -->
                            @else
                                <h3><a href="/login">Войдите</a>, чтобы оставить отзыв</h3>
                            @endif


                        </div>
                        <!-- /Write Review -->

                    </div>
                    <!-- /Reviews Content -->

                </div>
            </div>
        </div>
        <!-- /Doctor Details Tab -->

    </div>
</div>
@endsection
