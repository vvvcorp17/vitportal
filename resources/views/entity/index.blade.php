<?php $page = "map-list"; ?>
@extends('layout.mainlayout')
@section('content')
    <div class="content container" style="transform: none; min-height: 350.667px;">
        <div class="col-md-12 "style="overflow: visible; box-sizing: border-box; min-height: 1px;">
            <div class="">
                <div class="card ">
                    <div class="card-header" style="text-align: center">
                        <button type="button" class="show-search-properties btn btn-rounded btn-primary">
                            Поиск
                        </button>
                        @if($user && $user->business ?? false)
                            <button type="button" data-toggle="modal" href="#manage-entity" class="btn btn-rounded btn-primary">
                                Добавить
                            </button>
                        @endif
                    </div>

                    <div class="col-md-5 card-body search-properties" style="display: none; margin: 0 auto;">
                        <form action="/entities/{{ $category->slug }}" id="search-entity">
                            <div class="form-group card-label form-focus">
                                <label>Название</label>
                                <input type="text" autocomplete="off" class="form-control" name="title" value="{{ request()->get('title') }}">
                            </div>

                            <div class="filter-widget">
                                <div class="form-group card-label form-focus row" style="margin: 0 auto;">
                                    <div class="form-group card-label form-focus col-6 pl-1 pr-1">
                                        <label>цена от</label>
                                        <input
                                                autocomplete="off"
                                                type="number"
                                                name="priceFrom"
                                                value="{{ request()->get('priceFrom') }}"
                                                class="form-control floating"
                                        >
                                    </div>
                                    <div class="form-group card-label form-focus col-6 pl-1 pr-1">
                                        <label>цена до</label>
                                        <input
                                                autocomplete="off"
                                                type="number"
                                                name="priceTo"
                                                value="{{ request()->get('priceTo') }}"
                                                class="form-control floating"
                                        >
                                    </div>
                                </div>
                            </div>


                            @foreach($category->props as $prop)

                                @if (!$prop->filter) @continue; @endif

                                <div class="filter-widget">
                                    @if(in_array($prop->type, ['number', 'date']))
                                        <div class="form-group card-label form-focus row">
                                            <div class="form-group card-label form-focus col-6 pl-1 pr-1">
                                                <label>{{ $prop->title }} от</label>
                                                <input
                                                        autocomplete="off"
                                                        type="{{ $prop->type === 'date' ? 'date' : $prop->type }}"
                                                        name="filterRange[{{ $prop->id }}][from]"
                                                        value="{{ request()->get('filterRange')[$prop->id]['from'] ?? '' }}"
                                                        class="form-control floating"
                                                >
                                            </div>
                                            <div class="form-group card-label form-focus col-6 pl-1 pr-1">
                                                <label>{{ $prop->title }} до</label>
                                                <input
                                                        autocomplete="off"
                                                        type="{{ $prop->type === 'date_time' ? 'date' : $prop->type }}"
                                                        name="filterRange[{{ $prop->id }}][to]"
                                                        value="{{ request()->get('filterRange')[$prop->id]['to'] ?? '' }}"
                                                        class="form-control floating"
                                                >
                                            </div>
                                        </div>
                                    @elseif($prop->type === 'enum')
                                        <div class="form-group card-label form-focus pl-1 pr-1">
                                            <label>{{ $prop->title }}</label>
                                            <select name="filterIn[{{ $prop->id }}][]" class="select" multiple>
                                                @foreach($prop->enums as $enum)
                                                    <option
                                                            {{ isset(request()->get('filterIn')[$prop->id]) && in_array($enum->value, request()->get('filterIn')[$prop->id]) ? 'selected' : '' }}
                                                            value="{{ $enum->value }}">
                                                        {{ $enum->value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-group card-label form-focus pl-1 pr-1">
                                            <label>{{ $prop->title }}</label>
                                            <input
                                                    type="text"
                                                    autocomplete="off"
                                                    class="form-control"
                                                    name="filter[{{ $prop->id }}]"
                                                    value="{{ isset(request()->get('filter')[$prop->id]) ? request()->get('filter')[$prop->id] : '' }}"
                                            >
                                        </div>
                                    @endif
                                </div>

                            @endforeach

                            <div class="btn-search mt-5 center">
                                <a href="/entities/{{ $category->slug }}">
                                    <button class="btn">Очистить</button>
                                </a>
                                <button type="submit" class="btn">Найти</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="" style="transform: none;">
            <div class="">
                <div class="row">
                    @foreach($entities as $entity)
                        <div class="col-md-4 col-sm-4 col-lg-3">
                            @include('entity.entity', ['manage' => 1])
                        </div>
                    @endforeach
                </div>
                {{ $entities->onEachSide(3)->links() }}
            </div>
        </div>
    </div>

    @include('entity.entity-manage', ['entity' => null])

@endsection
