CREATE DATABASE vitportal;
CREATE USER root WITH PASSWORD 'secret';
GRANT ALL PRIVILEGES ON DATABASE 'vitportal' to root;


CREATE DATABASE vitportaltest;
CREATE USER root WITH PASSWORD 'secret';
GRANT ALL PRIVILEGES ON DATABASE 'vitportaltest' to root;
