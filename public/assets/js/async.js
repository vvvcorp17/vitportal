// rewrite form
body.on("submit", "form", function (e) {
    e.preventDefault();

    let form = $(this);

    if(form.attr("confirm") && !confirm('Вы уверены?')) {
        return false;
    }

    $.ajax({
        url: form.attr("action"),
        method: form.attr("method"),
        data: new FormData(form[0]),
        cache: false,
        contentType: false,
        processData: false,
    })
        .done(function(res){
            eval(form.attr("onsuccess"))
        })
        .fail(function(err) {
            //
        })
        .always(function(res) {
            $("#main-content").html(res);
        });
});

// rewrite a
body.on("click", "a", function (e) {

    let href = $(this).attr("href");

    if (
        !href
        || href.indexOf('uploads') !== -1
        || href.indexOf('#') !== -1
    ) {
        return ;
    }

    e.preventDefault();
    redirect(href)
})

function redirect(href) {
    $.get({
        url: href,
        data: {},
        headers: { 'async': 1 }
    }, function (res) {
        $("#main-content").html(res);
        changeURL(href);
    })
}

function changeURL(href) {
    history.pushState(href,href,href)
}
