let body = $("body");

$('.toast').toast('show');

function imgPreviewOnUpload() {
    $("#frames").html('');
    for (var i = 0; i < $(this)[0].files.length; i++) {
        $("#frames").append('<img src="'+window.URL.createObjectURL(this.files[i])+'" width="100px" height="100px"/>');
    }
}

function inputFloating() {
	if($('.floating').length > 0 ){
		$('.floating').on('focus blur', function (e) {
			$(this).parents('.form-focus').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
	}
}

inputFloating();

function imagesUpload(img) {
	if (img.files) {
		Object.values(img.files).forEach(function (cimg) {

			var reader = new FileReader();

			reader.onload = function(e) {
				$(img).next(".row").prepend(
					`<img class="img-preview" width="100px" src="${e.target.result}" alt="">`
				);
			};

			reader.readAsDataURL(cimg);
		})
	}
}

function imageUpload(img) {
	if (img.files && img.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$(img).closest("div").children("img").attr('src', e.target.result);
		}

		reader.readAsDataURL(img.files[0]); // convert to base64 string
	}
}

$(".mainImage").change(function() {
	imageUpload(this);
});

$(".uploadImages").change(function() {
	imagesUpload(this);
});


function removeImage(img) {
	let id = $(img).data("id");
	let image = $(img).data("image");
	let token = $(img).data("token");

	$.ajax({
		url:`/entities/${id}/remove-img`,
		type:"post",
		headers: {'X-CSRF-TOKEN': token},
		data: {
			image: image,
			_token: token
		},
		success: function(msg) {
			$(img).closest("div").remove()
		},
		error: function (msg) {
			alert('Error');
		}
	});
}

$(".entity-image-trash").click(function() {
	removeImage(this);
});


function drawPropertiesBycategory(category) {

	$.get('/properties/byCategory/' + category, function (res) {

		$("#propertyArea").html('');
		res.forEach(function(prop) {
			$("#propertyArea").append(
				`<div class="form-group form-focus">
                        <label class="focus-label">${prop.title}</label>
                        <input type="${ prop.type === 'int' ? 'number' : 'text' }" name="property_values[${prop.id}]" placeholder="${prop.title}" value="" class="form-control floating">
                    </div>`
			);

			inputFloating();
		})
	})
}


 function deleteHallImage(block, hall, image, _token) {

    confirm("Are you sure?")

     $.post( "/halls/remove-img", {
         hall,
         image,
         _token,
     }, function (res) {
         block.closest("div").remove()
     } );

 }


(function($) {
    "use strict";

    $(".show-search-properties").click(function (e) {
    	$(".search-properties").toggle('slow')
	})

	$("#search-entity").on('submit',function (e) {
		e.preventDefault()

		let filters = [];

		($("#search-entity").serializeArray()).forEach(function (filter) {
			if (filter.value) {
				filters.push(filter);
			}
		})

		window.location.href = $(this).attr("action")+'?'+$.param(filters);
	})


	// Stick Sidebar

    $(".nav-tabs a").on('click',function (){
        $(".nav-tabs a").removeClass('active');
        $(this).toggleClass('active');
    })

	if ($(window).width() > 767) {
		if($('.theiaStickySidebar').length > 0) {
			$('.theiaStickySidebar').theiaStickySidebar({
			  // Settings
			  additionalMarginTop: 30
			});
		}
	}

    $("a[href='"+ location.pathname +"']").closest("li").addClass("active")

	// Sidebar

	if($(window).width() <= 991){
	var Sidemenu = function() {
		this.$menuItem = $('.main-nav a');
	};

	function init() {
		var $this = Sidemenu;
		$('.main-nav a').on('click', function(e) {
			if($(this).parent().hasClass('has-submenu')) {
				e.preventDefault();
			}
			if(!$(this).hasClass('submenu')) {
				$('ul', $(this).parents('ul:first')).slideUp(350);
				$('a', $(this).parents('ul:first')).removeClass('submenu');
				$(this).next('ul').slideDown(350);
				$(this).addClass('submenu');
			} else if($(this).hasClass('submenu')) {
				$(this).removeClass('submenu');
				$(this).next('ul').slideUp(350);
			}
		});
	}

	// Sidebar Initiate
	init();
	}

	// Textarea Text Count

	var maxLength = 100;
	$('#review_desc').on('keyup change', function () {
		var length = $(this).val().length;
		 length = maxLength-length;
		$('#chars').text(length);
	});

	// Select 2

	if($('.select').length > 0) {
		$('.select').select2({
			minimumResultsForSearch: -1,
			width: '100%'
		});
	}

	// Date Time Picker

	if($('.datetimepicker').length > 0) {
		$('.datetimepicker').datetimepicker({
			dateFormat: "yy-mm-dd",
			timeFormat:  "hh:mm:ss",
			icons: {
				up: "fas fa-chevron-up",
				down: "fas fa-chevron-down",
				next: 'fas fa-chevron-right',
				previous: 'fas fa-chevron-left'
			}
		});
	}


	// Mobile menu sidebar overlay

	$('body').append('<div class="sidebar-overlay"></div>');
	$(document).on('click', '#mobile_btn', function() {
		$('main-wrapper').toggleClass('slide-nav');
		$('.sidebar-overlay').toggleClass('opened');
		$('html').addClass('menu-opened');
		return false;
	});

	$(document).on('click', '.sidebar-overlay', function() {
		$('html').removeClass('menu-opened');
		$(this).removeClass('opened');
		$('main-wrapper').removeClass('slide-nav');
	});

	$(document).on('click', '#menu_close, .main-menu-wrapper li:not(.has-submenu)', function() {
		$('html').removeClass('menu-opened');
		$('.sidebar-overlay').removeClass('opened');
		$('main-wrapper').removeClass('slide-nav');
	});

	// Tooltip

	if($('[data-toggle="tooltip"]').length > 0 ){
		$('[data-toggle="tooltip"]').tooltip();
	}

	// Add More Hours

    $(".hours-info").on('click','.trash', function () {
		$(this).closest('.hours-cont').remove();
		return false;
    });

    $(".add-hours").on('click', function () {

		var hourscontent = '<div class="row form-row hours-cont">' +
			'<div class="col-12 col-md-10">' +
				'<div class="row form-row">' +
					'<div class="col-12 col-md-6">' +
						'<div class="form-group">' +
							'<label>Start Time</label>' +
							'<select class="form-control">' +
								'<option>-</option>' +
								'<option>12.00 am</option>' +
								'<option>12.30 am</option>' +
								'<option>1.00 am</option>' +
								'<option>1.30 am</option>' +
							'</select>' +
						'</div>' +
					'</div>' +
					'<div class="col-12 col-md-6">' +
						'<div class="form-group">' +
							'<label>End Time</label>' +
							'<select class="form-control">' +
								'<option>-</option>' +
								'<option>12.00 am</option>' +
								'<option>12.30 am</option>' +
								'<option>1.00 am</option>' +
								'<option>1.30 am</option>' +
							'</select>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<div class="col-12 col-md-2"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>' +
		'</div>';

        $(".hours-info").append(hourscontent);
        return false;
    });

	// Content div min height set

	function resizeInnerDiv() {
		var height = $(window).height();
		var header_height = $(".header").height();
		var footer_height = $(".footer").height();
		var setheight = height - header_height;
		var trueheight = setheight - footer_height;
		$(".content").css("min-height", trueheight);
	}

	if($('.content').length > 0 ){
		resizeInnerDiv();
	}

	$(window).resize(function(){
		if($('.content').length > 0 ){
			resizeInnerDiv();
		}
	});

	// Slick Slider

	if($('.specialities-slider').length > 0) {
		$('.specialities-slider').slick({
			dots: true,
			autoplay:false,
			variableWidth: true,
			prevArrow: false,
			nextArrow: false
		});
	}

	if($('.doctor-slider').length > 0) {
		$('.doctor-slider').slick({
			dots: false,
			autoplay:false,
			infinite: false,
			variableWidth: true,
		});
	}
	if($('.features-slider').length > 0) {
		$('.features-slider').slick({
			dots: true,
			centerMode: true,
			slidesToShow: 3,
			speed: 500,
			variableWidth: true,
			arrows: false,
			autoplay:false,
			responsive: [{
				  breakpoint: 992,
				  settings: {
					slidesToShow: 4
				  }

			}]
		});
	}

	// Date Range Picker
	if($('.bookingrange').length > 0) {
		var start = moment().subtract(6, 'days');
		var end = moment();

		function booking_range(start, end) {
			$('.bookingrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('.bookingrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, booking_range);

		booking_range(start, end);
	}
	// Chat

	var chatAppTarget = $('.chat-window');
	(function() {
		if ($(window).width() > 991)
			chatAppTarget.removeClass('chat-slide');

		$(document).on("click",".chat-window .chat-users-list a.media",function () {
			if ($(window).width() <= 991) {
				chatAppTarget.addClass('chat-slide');
			}
			return false;
		});
		$(document).on("click","#back_user_list",function () {
			if ($(window).width() <= 991) {
				chatAppTarget.removeClass('chat-slide');
			}
			return false;
		});
	})();

	// Circle Progress Bar

	function animateElements() {
		$('.circle-bar1').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph1').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph1').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#da3f81'
					}
				});
			}
		});
		$('.circle-bar2').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph2').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph2').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#68dda9'
					}
				});
			}
		});
		$('.circle-bar3').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph3').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph3').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#1b5a90'
					}
				});
			}
		});
	}

	if($('.circle-bar').length > 0) {
		animateElements();
	}
	$(window).scroll(animateElements);

	// Preloader

	$(window).on('load', function () {
		if($('#loader').length > 0) {
			$('#loader').delay(350).fadeOut('slow');
			$('body').delay(350).css({ 'overflow': 'visible' });
		}
	})

})(jQuery);


function initMap(lat=46.85, lon=29.0, search = '') {
	map = new L.Map('map', {
		zoomControl: false,
		fullscreenControl: true,
		fullscreenControlOptions: {
			position: 'topleft'
		}
	}).setView([lat, lon], 7);

	// map.addControl(new L.Control.Fullscreen());

	// Icon = new L.icon({
	// 	iconUrl: '/assets/images/v.png',
	// 	iconAnchor: [17, 45],
	// 	popupAnchor:  [0, -28]
	// });
	//
	// myPositionIcon = new L.icon({
	// 	iconUrl: '/assets/images/v.png',
	// 	iconAnchor: [17, 45],
	// 	popupAnchor:  [0, -28]
	// });
	//
	// let myPositionIcon2 = new L.icon({
	// 	iconUrl: '/icons/v2.png',
	// 	iconAnchor: [17, 45],
	// 	popupAnchor:  [0, -28]
	// });


	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'VVVCorp',
		id: 'mapbox/streets-v11'
	}).addTo(map);

	L.control.zoom({
		position: 'bottomleft'
	}).addTo(map);


	if (search) {
		$.get('https://eu1.locationiq.com/v1/search.php?key=5f33a29a4264e8&q=' + search + '&format=json')
			.then((response) => {
				// removeAllLayers();
				//
				// response.forEach((item, key) => {
				//     let marker = L.marker([item.lat, item.lon], myPositionIcon2).addTo(map);
				//     marker.bindPopup(item.display_name);
				//     marker.on('click', onClick);
				// });

				map.flyTo([response[0].lat, response[0].lon], 7);
				L.marker([response[0].lat, response[0].lon]).addTo(map);
			});
	}
}
